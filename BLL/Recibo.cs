﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data; 
using DAL;

namespace BLL
{
    public class Recibo
    {

        DAL.MapperRecibo mp = new DAL.MapperRecibo();
        DAL.MapperConcepto mc = new DAL.MapperConcepto(); 


        public void Grabar(BE.Recibo recibo)
        {
            if (recibo.IdRec == 0)
            {
                mp.Insertar(recibo);

            }
          

        }

        public double calcularSueldoNeto(BE.Recibo recibo)
        {
            
            double Sueldo = recibo.SueldoBruto; 

            foreach ( BE.Concepto concepto  in recibo.Conceptos  )
            {
                 Sueldo += recibo.SueldoBruto * concepto.Porcentaje / 100;
            }

            //recibo.SueldoNeto = Sueldo;

            return Sueldo; 
        }

        private double calcularDescuentos(BE.Recibo recibo)
        {
            double descuentos = 0;

            foreach (BE.Concepto concepto in recibo.Conceptos)
            {
                if(concepto.Porcentaje < 0)
                    descuentos += recibo.SueldoBruto * concepto.Porcentaje / 100;
            }

            return descuentos;

        }


        public List<BE.Recibo> Listar(BE.Empleado empleado)
        {
            List < BE.Recibo > recibos = mp.Listar(empleado);

            foreach (BE.Recibo recibo in recibos)
            {

                recibo.SueldoNeto = calcularSueldoNeto(recibo);
                recibo.Descuento = calcularDescuentos(recibo);

            }
            return recibos;
        }

        public void GrabarConcepto(BE.Recibo recibo, BE.Concepto concepto)
        {
            mp.GrabarConcepto(recibo, concepto);
        }
    }
}
