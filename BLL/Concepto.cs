﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
   public  class Concepto
    {
        DAL.MapperConcepto mp = new DAL.MapperConcepto();



        public void Grabar(BE.Concepto concepto)
        {
            if (concepto.Id == 0)
            {
                mp.Insertar(concepto);

            }
            else
            {
                mp.Editar(concepto);
            }

        }

        public void Borrar(BE.Concepto concepto)
        {
            mp.Borrar(concepto);
        }

        public List<BE.Concepto> Listar()
        {
            return mp.Listar();

        }




    }

}

