﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DAL; 
namespace BLL
{
    public class Empleado
    {
        DAL.MapperEmpleado mp = new DAL.MapperEmpleado();



        public void Grabar(BE.Empleado empleado)
        {
            if (empleado.Legajo == 0)
            {
                mp.Insertar(empleado);

            }
            else
            {
                mp.Editar(empleado);
            }

        }

        public void Borrar(BE.Empleado empleado)
        {
            mp.Borrar(empleado);
        }

        public List<BE.Empleado> Listar()
        {
            return mp.Listar();

        }




    }
}
