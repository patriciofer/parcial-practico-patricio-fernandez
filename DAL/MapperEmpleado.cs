﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
   public  class MapperEmpleado
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Empleado empleado)
        {
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nom", empleado.Nombre));
            parameters.Add(acceso.CrearParametro("@Ape", empleado.Apellido));
            parameters.Add(acceso.CrearParametro("@Cuil", empleado.Cuil));
            parameters.Add(acceso.CrearParametro("@Fecha", empleado.FechaAlta));
            acceso.Escribir("Insertar_Empleado", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Empleado empleado)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();


            parameters.Add(acceso.CrearParametro("@Legajo", empleado.Legajo));
            parameters.Add(acceso.CrearParametro("@Nom", empleado.Nombre));
            parameters.Add(acceso.CrearParametro("@Ape", empleado.Apellido));
            parameters.Add(acceso.CrearParametro("@Cuil", empleado.Cuil));
            parameters.Add(acceso.CrearParametro("@Fecha", empleado.FechaAlta));
            acceso.Escribir("Modificar_Empleado", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Empleado empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.Legajo));
            acceso.Escribir("Borrar_Empleado", parameters);

            acceso.Cerrar();
        }

        public List<BE.Empleado> Listar()
        {
            List<BE.Empleado> lista = new List<BE.Empleado>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Empleado");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Empleado p = new BE.Empleado();
                p.Nombre = registro[1].ToString();
                p.Legajo = int.Parse(registro[0].ToString());
                p.Apellido = registro[2].ToString();
                p.Cuil = registro[3].ToString();
                p.FechaAlta = DateTime.Parse(registro[4].ToString());
                lista.Add(p);
            }
            return lista;
        }
    }

}

