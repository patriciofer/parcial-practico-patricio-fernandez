﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class MapperConcepto
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Concepto concepto)
        {
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Desc", concepto.Descripcion));
            parameters.Add(acceso.CrearParametro("@Porc", concepto.Porcentaje));
             
            acceso.Escribir("Insertar_Concepto", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Concepto concepto)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();


            parameters.Add(acceso.CrearParametro("@Id", concepto.Id));
            parameters.Add(acceso.CrearParametro("@Desc", concepto.Descripcion));
            parameters.Add(acceso.CrearParametro("@Porc", concepto.Porcentaje));
            acceso.Escribir("Modificar_Concepto", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();


            parameters.Add(acceso.CrearParametro("@Id", concepto.Id));
            acceso.Escribir("Borrar_Concepto", parameters);

            acceso.Cerrar();
        }

        public List<BE.Concepto> Listar(BE.Recibo recibo)
        {
            List<BE.Concepto> lista = new List<BE.Concepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

           
            parameters.Add(acceso.CrearParametro("@IdRec", recibo.IdRec));
            //Comento lo que pincha



            //DataTable tabla = acceso.Leer("Listar_Concepto", parameters);
            DataTable tabla = acceso.Leer("Listar_ReciboID", parameters);
          
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto p = new BE.Concepto();
                p.Descripcion= registro[1].ToString();
                p.Id = int.Parse(registro[0].ToString());
                p.Porcentaje= int.Parse(registro[2].ToString());
                lista.Add(p);
            }
            return lista;
        }


        public List<BE.Concepto> Listar()
        {
            List<BE.Concepto> lista = new List<BE.Concepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            


            DataTable tabla = acceso.Leer("Listar_Concepto");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto p = new BE.Concepto();
                p.Descripcion = registro[1].ToString();
                p.Id = int.Parse(registro[0].ToString());
                p.Porcentaje = int.Parse(registro[2].ToString());
                lista.Add(p);
            }
            return lista;
        }

    }

}

