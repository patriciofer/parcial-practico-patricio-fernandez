﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient; 


namespace DAL
{
   public  class MapperRecibo
    {

        private Acceso acceso = new Acceso();

        public void Insertar(BE.Recibo recibo)
        {
            acceso.Abrir();

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            //parameters.Add(acceso.CrearParametro("@IdRec", recibo.IdRec));
            parameters.Add(acceso.CrearParametro("@Fliq", recibo.FechaLiq));
            parameters.Add(acceso.CrearParametro("@SueldoB", recibo.SueldoBruto));
            parameters.Add(acceso.CrearParametro("@SueldoN", recibo.SueldoNeto));
            parameters.Add(acceso.CrearParametro("@Legajo", recibo.empleado.Legajo));



            acceso.Escribir("Insertar_Recibo", parameters);

            acceso.Cerrar();
        }

        public List<BE.Recibo> Listar(BE.Empleado empleado)
        {
            List<BE.Recibo> lista = new List<BE.Recibo>();

            MapperConcepto mapperConcepto = new MapperConcepto();

           

            Acceso acceso = new Acceso();
            acceso.Abrir();


            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Legajo", empleado.Legajo));

            DataTable tabla = acceso.Leer("Listar_Recibo", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo r = new BE.Recibo();
                r.FechaLiq = DateTime.Parse(registro[1].ToString());
                r.SueldoBruto = double.Parse(registro[2].ToString());
                r.IdRec = int.Parse(registro[0].ToString());
                //p.TotalDescuento
                //p.concepto.Descripcion = registro[5].ToString();
                r.Conceptos = mapperConcepto.Listar(r);
                lista.Add(r);
            }
            return lista;
        }

        public void GrabarConcepto(BE.Recibo recibo, BE.Concepto concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", concepto.Id));
            parameters.Add(acceso.CrearParametro("@IdRec", recibo.IdRec));
            acceso.Escribir("Insertar_Recibo_Concepto", parameters);
            acceso.Cerrar();
        }


    }
}
