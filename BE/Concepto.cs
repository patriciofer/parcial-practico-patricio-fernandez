﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Concepto
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private int porcentaje;

        public int Porcentaje
        {
            get { return porcentaje; }
            set { porcentaje = value; }
        }

    }
}
