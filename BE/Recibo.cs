﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Recibo
    {


        private int idRec;

        public int IdRec
        {
            get { return idRec; }
            set { idRec = value; }
        }


        private DateTime fechaLiq;

        public DateTime FechaLiq
        {
            get { return fechaLiq; }
            set { fechaLiq = value; }
        }

        private double sueldoBruto;

        public double SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private double sueldoneto;
        public double SueldoNeto
        {
            get { return sueldoneto; }
            set { sueldoneto = value; }
        }


        public Empleado empleado { get; set; }




        private List<Concepto> conceptos;

        public List<Concepto> Conceptos
        {
            get { return conceptos; }
            set { conceptos = value; }
        }


        private double descuento;

        public double Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }



    }
}
