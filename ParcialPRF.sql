USE [MyCompany]
GO
/****** Object:  Table [dbo].[Concepto1]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Concepto1](
	[Id] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[Porcentaje] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado1]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado1](
	[Legajo] [int] NOT NULL,
	[Nombre] [nvarchar](50) NULL,
	[Apellido] [nvarchar](50) NULL,
	[Cuil] [nvarchar](50) NULL,
	[FechaAlta] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo1]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo1](
	[IdRecibo] [int] NOT NULL,
	[FechaLiq] [datetime] NULL,
	[SueldoBruto] [numeric](18, 0) NOT NULL,
	[Conceptos] [nvarchar](50) NULL,
	[SueldoNeto] [numeric](18, 0) NULL,
	[TotalDescuento] [numeric](18, 0) NULL,
	[legajo] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReciboConcepto1]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReciboConcepto1](
	[id] [int] NOT NULL,
	[IdRecibo] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Borrar_Concepto]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Borrar_Concepto]
@Id int as
BEGIN

	delete from Concepto1
	where Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[Borrar_Empleado]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Borrar_Empleado]
@Legajo int as
BEGIN

	delete from Empleado1
	where legajo = @Legajo
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Concepto]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Insertar_Concepto] 
@Desc nvarchar (50) , @Porc int
as
BEGIN

Declare @Id int
set @Id   = isnull((select max(Id) from Concepto1),0) +1

insert into Concepto1 values(@Id, @Desc,@Porc)
 
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Empleado]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Insertar_Empleado] 
@Nom nvarchar (50) , @Ape nvarchar(50),@Cuil nvarchar(50), @Fecha datetime 
as
BEGIN

Declare @legajo int
set @legajo   = isnull((select max(legajo) from Empleado1),0) +1

insert into Empleado1 values(@legajo, @Nom,@Ape,@cuil,@fecha )
 
END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Recibo]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Insertar_Recibo] 
  @Fliq datetime, @SueldoB numeric (18,0), @Sueldon numeric (18,0), @Legajo int
as
BEGIN

 


 
Declare @IdRec int
set @IdRec   = isnull((select max(IdRecibo) from Recibo1),0) +1

insert into Recibo1(IdRecibo, FechaLiq,SueldoBruto, SueldoNeto, legajo) values(@IdRec, @Fliq, @SueldoB, @Sueldon, @Legajo)
 


END
GO
/****** Object:  StoredProcedure [dbo].[Insertar_Recibo_Concepto]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Insertar_Recibo_Concepto] 
 @id int, @idRec int 
as
BEGIN

 

insert into ReciboConcepto1 values(@Id, @idRec)
 
END
GO
/****** Object:  StoredProcedure [dbo].[Listar_Concepto]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_Concepto]
as 
begin
select * from dbo.Concepto1
end

GO
/****** Object:  StoredProcedure [dbo].[Listar_Empleado]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_Empleado]
as 
begin
select * from dbo.Empleado1
end
GO
/****** Object:  StoredProcedure [dbo].[Listar_Recibo]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_Recibo]
@legajo int
as 
begin
select * from 
[dbo].Recibo1  where legajo= @legajo


end 
GO
/****** Object:  StoredProcedure [dbo].[Listar_ReciboID]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Listar_ReciboID]
@idRec int 
as 
begin
 

select a.* from Concepto1 a
inner join ReciboConcepto1 b
on a.id = B.id
WHERE b.idrecibo= @idRec



end
GO
/****** Object:  StoredProcedure [dbo].[Modificar_Concepto]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Modificar_Concepto] 
@Id int , @Desc nvarchar (50) , @Porc int
as
BEGIN

update Concepto1 set
Descripcion=@Desc,
Porcentaje = @Porc
 where Id = @id
 
END
GO
/****** Object:  StoredProcedure [dbo].[Modificar_Empleado]    Script Date: 29/09/2020 11:32:32 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Modificar_Empleado] 
@Legajo int , @Nom nvarchar (50) , @Ape varchar(50),@Cuil varchar(50), @Fecha datetime
as
BEGIN

update Empleado1 set
Nombre= @Nom,
Apellido=@Ape,
Cuil = @Cuil,
FechaAlta=@fecha
where legajo = @legajo
 
END
GO
