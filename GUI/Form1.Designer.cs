﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtLegajo = new System.Windows.Forms.TextBox();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtApellido = new System.Windows.Forms.TextBox();
            this.TxtCuil = new System.Windows.Forms.TextBox();
            this.TxtFecha = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btndeleteEmpl = new System.Windows.Forms.Button();
            this.btnUpdateEmpl = new System.Windows.Forms.Button();
            this.btnInsertEmpl = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtPorDesc = new System.Windows.Forms.TextBox();
            this.TxtDescCon = new System.Windows.Forms.TextBox();
            this.TxtIdCon = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btnDeletecon = new System.Windows.Forms.Button();
            this.btnUpdateCon = new System.Windows.Forms.Button();
            this.btnInsertCon = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.btnInsertaRecibo = new System.Windows.Forms.Button();
            this.btnInsertarConcepto = new System.Windows.Forms.Button();
            this.TxtFechaRecibo = new System.Windows.Forms.TextBox();
            this.TxtSueldoBruto = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtLegajo
            // 
            this.TxtLegajo.Enabled = false;
            this.TxtLegajo.Location = new System.Drawing.Point(94, 33);
            this.TxtLegajo.Name = "TxtLegajo";
            this.TxtLegajo.Size = new System.Drawing.Size(100, 20);
            this.TxtLegajo.TabIndex = 0;
            this.TxtLegajo.TextChanged += new System.EventHandler(this.txtlegajo_TextChanged);
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(94, 59);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(100, 20);
            this.TxtNombre.TabIndex = 1;
            this.TxtNombre.TextChanged += new System.EventHandler(this.txtnombre_TextChanged);
            this.TxtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnombre_KeyPress);
            // 
            // TxtApellido
            // 
            this.TxtApellido.Location = new System.Drawing.Point(94, 85);
            this.TxtApellido.Name = "TxtApellido";
            this.TxtApellido.Size = new System.Drawing.Size(100, 20);
            this.TxtApellido.TabIndex = 2;
            this.TxtApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtapellido_KeyPress);
            // 
            // TxtCuil
            // 
            this.TxtCuil.Location = new System.Drawing.Point(94, 111);
            this.TxtCuil.Name = "TxtCuil";
            this.TxtCuil.Size = new System.Drawing.Size(100, 20);
            this.TxtCuil.TabIndex = 3;
            // 
            // TxtFecha
            // 
            this.TxtFecha.Location = new System.Drawing.Point(94, 137);
            this.TxtFecha.Name = "TxtFecha";
            this.TxtFecha.Size = new System.Drawing.Size(100, 20);
            this.TxtFecha.TabIndex = 4;
            this.TxtFecha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtfecha_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Legajo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Apellido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cuil";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Fecha  de Alta";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.btndeleteEmpl);
            this.groupBox1.Controls.Add(this.btnUpdateEmpl);
            this.groupBox1.Controls.Add(this.btnInsertEmpl);
            this.groupBox1.Controls.Add(this.TxtLegajo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtNombre);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtApellido);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtCuil);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtFecha);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(572, 325);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ABM Empleado";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 163);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(558, 147);
            this.dataGridView1.TabIndex = 14;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // btndeleteEmpl
            // 
            this.btndeleteEmpl.Location = new System.Drawing.Point(291, 95);
            this.btndeleteEmpl.Name = "btndeleteEmpl";
            this.btndeleteEmpl.Size = new System.Drawing.Size(75, 23);
            this.btndeleteEmpl.TabIndex = 13;
            this.btndeleteEmpl.Text = "Delete";
            this.btndeleteEmpl.UseVisualStyleBackColor = true;
            this.btndeleteEmpl.Click += new System.EventHandler(this.btndeleteEmpl_Click);
            // 
            // btnUpdateEmpl
            // 
            this.btnUpdateEmpl.Location = new System.Drawing.Point(291, 66);
            this.btnUpdateEmpl.Name = "btnUpdateEmpl";
            this.btnUpdateEmpl.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateEmpl.TabIndex = 12;
            this.btnUpdateEmpl.Text = "Update";
            this.btnUpdateEmpl.UseVisualStyleBackColor = true;
            this.btnUpdateEmpl.Click += new System.EventHandler(this.btnUpdateEmpl_Click);
            // 
            // btnInsertEmpl
            // 
            this.btnInsertEmpl.Location = new System.Drawing.Point(291, 35);
            this.btnInsertEmpl.Name = "btnInsertEmpl";
            this.btnInsertEmpl.Size = new System.Drawing.Size(75, 23);
            this.btnInsertEmpl.TabIndex = 11;
            this.btnInsertEmpl.Text = "Insert";
            this.btnInsertEmpl.UseVisualStyleBackColor = true;
            this.btnInsertEmpl.Click += new System.EventHandler(this.btnInsertEmpl_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtPorDesc);
            this.groupBox2.Controls.Add(this.TxtDescCon);
            this.groupBox2.Controls.Add(this.TxtIdCon);
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Controls.Add(this.btnDeletecon);
            this.groupBox2.Controls.Add(this.btnUpdateCon);
            this.groupBox2.Controls.Add(this.btnInsertCon);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(646, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(424, 324);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ABM Concepto";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // TxtPorDesc
            // 
            this.TxtPorDesc.Location = new System.Drawing.Point(100, 88);
            this.TxtPorDesc.Name = "TxtPorDesc";
            this.TxtPorDesc.Size = new System.Drawing.Size(100, 20);
            this.TxtPorDesc.TabIndex = 9;
            // 
            // TxtDescCon
            // 
            this.TxtDescCon.Location = new System.Drawing.Point(100, 62);
            this.TxtDescCon.Name = "TxtDescCon";
            this.TxtDescCon.Size = new System.Drawing.Size(100, 20);
            this.TxtDescCon.TabIndex = 8;
            // 
            // TxtIdCon
            // 
            this.TxtIdCon.Enabled = false;
            this.TxtIdCon.Location = new System.Drawing.Point(100, 36);
            this.TxtIdCon.Name = "TxtIdCon";
            this.TxtIdCon.Size = new System.Drawing.Size(100, 20);
            this.TxtIdCon.TabIndex = 7;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(27, 162);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(343, 150);
            this.dataGridView2.TabIndex = 6;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // btnDeletecon
            // 
            this.btnDeletecon.Location = new System.Drawing.Point(279, 90);
            this.btnDeletecon.Name = "btnDeletecon";
            this.btnDeletecon.Size = new System.Drawing.Size(75, 23);
            this.btnDeletecon.TabIndex = 5;
            this.btnDeletecon.Text = "Delete";
            this.btnDeletecon.UseVisualStyleBackColor = true;
            this.btnDeletecon.Click += new System.EventHandler(this.btnDeletecon_Click);
            // 
            // btnUpdateCon
            // 
            this.btnUpdateCon.Location = new System.Drawing.Point(279, 61);
            this.btnUpdateCon.Name = "btnUpdateCon";
            this.btnUpdateCon.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateCon.TabIndex = 4;
            this.btnUpdateCon.Text = "Update";
            this.btnUpdateCon.UseVisualStyleBackColor = true;
            this.btnUpdateCon.Click += new System.EventHandler(this.btnUpdateCon_Click);
            // 
            // btnInsertCon
            // 
            this.btnInsertCon.Location = new System.Drawing.Point(279, 29);
            this.btnInsertCon.Name = "btnInsertCon";
            this.btnInsertCon.Size = new System.Drawing.Size(75, 23);
            this.btnInsertCon.TabIndex = 3;
            this.btnInsertCon.Text = "Insert";
            this.btnInsertCon.UseVisualStyleBackColor = true;
            this.btnInsertCon.Click += new System.EventHandler(this.btnInsertCon_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(33, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Procentaje";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Descripcion";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Id";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(6, 79);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(556, 122);
            this.dataGridView3.TabIndex = 12;
            this.dataGridView3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellClick);
            // 
            // btnInsertaRecibo
            // 
            this.btnInsertaRecibo.Location = new System.Drawing.Point(362, 15);
            this.btnInsertaRecibo.Name = "btnInsertaRecibo";
            this.btnInsertaRecibo.Size = new System.Drawing.Size(200, 23);
            this.btnInsertaRecibo.TabIndex = 13;
            this.btnInsertaRecibo.Text = "Insertar Recibo";
            this.btnInsertaRecibo.UseVisualStyleBackColor = true;
            this.btnInsertaRecibo.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnInsertarConcepto
            // 
            this.btnInsertarConcepto.Location = new System.Drawing.Point(362, 44);
            this.btnInsertarConcepto.Name = "btnInsertarConcepto";
            this.btnInsertarConcepto.Size = new System.Drawing.Size(200, 23);
            this.btnInsertarConcepto.TabIndex = 14;
            this.btnInsertarConcepto.Text = "Insertar Concepto a Recibo";
            this.btnInsertarConcepto.UseVisualStyleBackColor = true;
            this.btnInsertarConcepto.Click += new System.EventHandler(this.button2_Click);
            // 
            // TxtFechaRecibo
            // 
            this.TxtFechaRecibo.Location = new System.Drawing.Point(106, 23);
            this.TxtFechaRecibo.Name = "TxtFechaRecibo";
            this.TxtFechaRecibo.Size = new System.Drawing.Size(100, 20);
            this.TxtFechaRecibo.TabIndex = 15;
            // 
            // TxtSueldoBruto
            // 
            this.TxtSueldoBruto.Location = new System.Drawing.Point(106, 53);
            this.TxtSueldoBruto.Name = "TxtSueldoBruto";
            this.TxtSueldoBruto.Size = new System.Drawing.Size(100, 20);
            this.TxtSueldoBruto.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Fecha liquidacion";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Sueldo Bruto";
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(661, 79);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(338, 125);
            this.dataGridView4.TabIndex = 19;
            this.dataGridView4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellClick);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(667, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Lista de Conceptos";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnInsertaRecibo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.dataGridView3);
            this.groupBox3.Controls.Add(this.dataGridView4);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.btnInsertarConcepto);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.TxtFechaRecibo);
            this.groupBox3.Controls.Add(this.TxtSueldoBruto);
            this.groupBox3.Location = new System.Drawing.Point(12, 343);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1066, 216);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Recibos";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 571);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TxtLegajo;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtApellido;
        private System.Windows.Forms.TextBox TxtCuil;
        private System.Windows.Forms.TextBox TxtFecha;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btndeleteEmpl;
        private System.Windows.Forms.Button btnUpdateEmpl;
        private System.Windows.Forms.Button btnInsertEmpl;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtPorDesc;
        private System.Windows.Forms.TextBox TxtDescCon;
        private System.Windows.Forms.TextBox TxtIdCon;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnDeletecon;
        private System.Windows.Forms.Button btnUpdateCon;
        private System.Windows.Forms.Button btnInsertCon;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button btnInsertaRecibo;
        private System.Windows.Forms.Button btnInsertarConcepto;
        private System.Windows.Forms.TextBox TxtFechaRecibo;
        private System.Windows.Forms.TextBox TxtSueldoBruto;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

