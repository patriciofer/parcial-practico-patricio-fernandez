﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;
using BE;
namespace GUI
{
    public partial class Form1 : Form
    {

        BLL.Empleado gestor = new BLL.Empleado();
        BLL.Concepto gestorconcepto = new BLL.Concepto();
        BLL.Recibo gestorRecibo = new BLL.Recibo();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
  

            Enlazar();

           
             
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestor.Listar();

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestorconcepto.Listar();


        }
        private void btnInsertEmpl_Click(object sender, EventArgs e)
        {
            if (!ValidarEmpleado())
                return;
 
            BE.Empleado p = new BE.Empleado();
            p.Nombre =TxtNombre.Text;
            p.Apellido = TxtApellido.Text;
            p.Cuil = TxtCuil.Text;
            p.FechaAlta = DateTime.Parse(TxtFecha.Text);
            gestor.Grabar(p);
            Enlazar();
        }

        private bool ValidarEmpleado()
        {
            if(string.IsNullOrEmpty(TxtNombre.Text))
            {
                MessageBox.Show("Ingrese el nombre del empleado");
                return false;
            }

            if (string.IsNullOrEmpty(TxtApellido.Text))
            {
                MessageBox.Show("Ingrese el Apellido del empleado");
                return false;
            }


            if (string.IsNullOrEmpty(TxtCuil.Text))
            {
                MessageBox.Show("Ingrese el Cuil del empleado");
                return false;
            }

            DateTime aux = DateTime.Now;

            if (!DateTime.TryParse(TxtFecha.Text, out aux))
            {
                MessageBox.Show("Ingrese la fecha de alta del empleado");
                return false;
            }



            return true;
        }

        public bool ValidarConcepto()
        {
            if (string.IsNullOrEmpty(TxtDescCon.Text))
            {
                MessageBox.Show("Ingrese descripcion de concepto");
                return false;
            }

            if (string.IsNullOrEmpty(TxtPorDesc.Text))
            {
                MessageBox.Show("Ingrese Porcentaje de concepto");
                return false;
            }



            return true;
        }


        public bool ValidarRecibo()
        {
            DateTime aux = DateTime.Now;

            if (!DateTime.TryParse(TxtFechaRecibo.Text, out aux))
            {
                MessageBox.Show("Ingrese la fecha de recibo");
                return false;
            }

            if (string.IsNullOrEmpty(TxtSueldoBruto.Text))
            {
                MessageBox.Show("Ingrese El sueldo Bruto");
                return false;
            }



            return true;
        }



        private void btnUpdateEmpl_Click(object sender, EventArgs e)
        {

            BE.Empleado p = new BE.Empleado();
            p.Legajo = int.Parse(TxtLegajo.Text);
            p.Nombre = TxtNombre.Text;
            p.Apellido = TxtApellido.Text;
            p.Cuil = TxtCuil.Text;
            p.FechaAlta = DateTime.Parse(TxtFecha.Text);
            gestor.Grabar(p);

            Enlazar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtLegajo.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            TxtNombre.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            TxtApellido.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            TxtCuil.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            TxtFecha.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            enlazarRecibos();
        }

        private void btndeleteEmpl_Click(object sender, EventArgs e)
        {
            BE.Empleado p = new BE.Empleado();

            p.Legajo = int.Parse(TxtLegajo.Text);

            gestor.Borrar(p);
            Enlazar();
        }

        private void txtlegajo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtnombre_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true; 
            }
        }

        private void txtapellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtfecha_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btnInsertCon_Click(object sender, EventArgs e)
        {
            if (!ValidarConcepto())
                return;

            BE.Concepto p = new BE.Concepto();
            p.Descripcion = TxtDescCon.Text;
            p.Porcentaje = int.Parse(TxtPorDesc.Text);
            
            gestorconcepto.Grabar(p);
            Enlazar();
        }

        private void btnUpdateCon_Click(object sender, EventArgs e)
        {
            BE.Concepto p = new BE.Concepto();
            p.Id = int.Parse(TxtIdCon.Text);
            p.Descripcion = TxtDescCon.Text;
            p.Porcentaje = int.Parse(TxtPorDesc.Text);

            gestorconcepto.Grabar(p);

            Enlazar();
        }

        private void btnDeletecon_Click(object sender, EventArgs e)
        {
            BE.Concepto p = new BE.Concepto();
            p.Id = int.Parse(TxtIdCon.Text);
            

            gestorconcepto.Borrar(p);

            Enlazar();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtIdCon.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            TxtDescCon.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            TxtPorDesc.Text = dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString();
        }


        private void enlazarConceptos ()
        {
            BE.Recibo recibo = (BE.Recibo)dataGridView3.SelectedRows[0].DataBoundItem;
            if (recibo == null)
                return;

            dataGridView4.DataSource = null;
          dataGridView4.DataSource = recibo.Conceptos; 
        }



        private void enlazarRecibos()
        {
            BE.Empleado empleado = (BE.Empleado)dataGridView1.SelectedRows[0].DataBoundItem;
            if (empleado == null)
                return;

            dataGridView3.DataSource = null;
            dataGridView3.DataSource = gestorRecibo.Listar(empleado);

            //enlazarConceptos();

        }

        // realizar alta recibo sin conceptos
        private void button1_Click(object sender, EventArgs e)
        {
            if (!ValidarRecibo())
                return;

            BE.Empleado empleado = (BE.Empleado)dataGridView1.SelectedRows[0].DataBoundItem;
            if (empleado == null)
                return;
            BE.Recibo recibo = new BE.Recibo();
            recibo.empleado = empleado;
            recibo.FechaLiq = DateTime.Parse(TxtFechaRecibo.Text);
            recibo.SueldoBruto = Double.Parse(TxtSueldoBruto.Text);
            gestorRecibo.Grabar(recibo);
            enlazarRecibos();
        }

        // agregar concepto a recibo
        private void button2_Click(object sender, EventArgs e)
        {
            BE.Recibo recibo = (BE.Recibo)dataGridView3.SelectedRows[0].DataBoundItem;
            if (recibo == null)
                return;

            BE.Concepto concepto = (BE.Concepto)dataGridView2.SelectedRows[0].DataBoundItem;
            if (concepto == null)
                return;
            gestorRecibo.GrabarConcepto(recibo, concepto);

            enlazarRecibos();
            enlazarConceptos();
        }

        private void dataGridView4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            enlazarConceptos();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
    }
}
